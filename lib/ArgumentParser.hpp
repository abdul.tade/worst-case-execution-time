#pragma once

#include <iostream>
#include <span>
#include <cstring>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

struct Arguments {

    static Arguments make_arguments(const std::unordered_set<std::string> &required, const std::unordered_set<std::string> &optionals) {
        return Arguments{required, optionals};
    }

    Arguments(const std::unordered_set<std::string> &required, const std::unordered_set<std::string> &optionals)
        : required(required),
          optionals(optionals)
    {}

    bool isRequired(const char* argName)
    {
        return required.find(std::string{argName}) != required.end();
    }

    bool isOptional(const char* argName) {
        return optionals.find(argName) != optionals.end();
    }

    bool argumentExists(const char* argName) {
        return isRequired(argName) || isOptional(argName);
    }

    std::unordered_set<std::string> getArguments() {
        auto r = required;
        for (auto &p : optionals) {
            if (r.count(p)) {
                std::cout << "Argument " << p << " must be optional or required only\n";
                std::exit(1);
            }
            r.insert(p);
        }
        return r;
    }

private:
    std::unordered_set<std::string> required;
    std::unordered_set<std::string> optionals;
};

struct ArgumentParser
{
    using str = const char *;

    ArgumentParser(size_t maxArgs, const std::unordered_set<std::string> &required, const std::unordered_set<std::string> &optionals)
        : maxArgs(maxArgs),
          arguments(Arguments::make_arguments(required, optionals))
    {}

    void parseArgs(std::span<char*> &ap)
    {
        if (ap.size() < maxArgs) {
            std::cout << "[-] Insufficient number of arguments\n";
            std::exit(1);
        }

        auto argSize = ap.size() - 1;
        if ((argSize == 0) || argSize % 2 != 0) {
            std::cout << "[-] Invalid argument format\nCorrect format: [PROG_NAME] [optname1] [optValue1] [optname2] [optValue2] ...";
            std::exit(1);
        }

        for (auto option : arguments.getArguments()) {

            auto it = std::find_if(ap.begin(), ap.end(), [&](char* value) {
                return option == value;
            });

            if (arguments.isRequired(option.c_str()) && it == ap.end()) {
                std::cout << "[-] Cannot find required option: " << option << std::endl;
                std::exit(1);
            }

            if (arguments.isOptional(option.c_str()) && it == ap.end()) {
                continue;
            }

            auto indx = std::distance(ap.begin(), it);
            optVal[ap[indx]] = ap[indx + 1];
        }
    }

    bool argumentExist(str optName) {
        return arguments.argumentExists(optName);
    }

    template <typename T>
    T getValue(str optName)
    {
        std::cout << "[-] On line " <<  __LINE__ << "in file " 
                  << __FILE__   << ": Unsupported getValue() type, terminating ...\n";
        std::exit(1);
    }

    template <>
    int getValue<int>(str optName)
    {
        auto &val = getArg(optName);
        return std::atoi(val.c_str());
    }

    template <>
    unsigned long getValue<unsigned long>(str optName)
    {
        auto &val = getArg(optName);
        return std::strtoul(val.c_str(), nullptr, 10);
    }

    template <>
    unsigned long long getValue<unsigned long long>(str optName)
    {
        auto &val = getArg(optName);
        return std::strtoul(val.c_str(), nullptr, 10);
    }

    template <>
    double getValue<double>(str optName)
    {
        auto &val = getArg(optName);
        return std::strtod(val.c_str(), nullptr);
    }

    template <>
    float getValue<float>(str optName)
    {
        auto &val = getArg(optName);
        return std::strtof(val.c_str(), nullptr);
    }

    template <>
    char getValue<char>(str optName)
    {
        auto &val = getArg(optName);
        return val[0];
    }

    template <>
    std::string getValue<std::string>(str optName) {
        return getArg(optName);
    }

    template <>
    void getValue<>(str optName)
    {
    }

private:
    std::string &getArg(str optName) {
        if (search(optName)) {
            return optVal.at(optName);
        }
        else if (arguments.isOptional(optName)) {
            std::cout << "Cannot find optional argument: " << optName << '\n';
            std::exit(1);
        }
        std::cout << "[-] Cannot find option: " << optName << '\n';
        std::exit(1);
    }

    bool search(str optName)
    {
        return arguments.argumentExists(optName);
    }

private:
    std::unordered_map<std::string, std::string> optVal;
    Arguments arguments;
    size_t maxArgs;
};