#pragma once

#include <cstdio>
#include <inttypes.h>
#include <Zydis/Zydis.h>
#include <unordered_map>

struct ClockCycle
{

    using OpcodeType = ZyanU8;

    ClockCycle() = default;

    int getInstrClkCycle(OpcodeType opcode) {
        return clockTable_.at(opcode);
    }

private:
    std::unordered_map<OpcodeType, int> clockTable_;
};