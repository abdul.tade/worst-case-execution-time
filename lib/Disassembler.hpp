#pragma once

#include <cstdio>
#include <inttypes.h>
#include <vector>
#include <Zydis/Zydis.h>
#include "../lib/ElFile.hpp"

struct Disassembler
{

    Disassembler(ElFile &file)
        : file_(file)
    {
        auto isX64 = file.isX64();
        auto machineMode = isX64 ? ZYDIS_MACHINE_MODE_LONG_64 : ZYDIS_MACHINE_MODE_LEGACY_32;
        auto addressWidth = isX64 ? ZYDIS_ADDRESS_WIDTH_64 : ZYDIS_ADDRESS_WIDTH_32;
        ZydisDecoderInit(&decoder_, machineMode, addressWidth);
    }

    std::vector<ZydisDecodedInstruction> decodeInstructions()
    {
        std::vector<ZydisDecodedInstruction> decodedInstructions{};
        const auto tsection = file_.getTextSection();
        ZyanU64 runtimeAddr = (ZyanU64)tsection.data();
        ZyanUSize length = tsection.size();
        auto sectionData = reinterpret_cast<const char*>(tsection.data());
        ZyanUSize offset = 0;
        ZydisDecodedInstruction instruction;

        while (ZYAN_SUCCESS(ZydisDecoderDecodeBuffer(&decoder_, sectionData + offset, length, &instruction)))
        {
            decodedInstructions.emplace_back(instruction);
            offset += instruction.length;
        }

        return decodedInstructions;
    }

private:
    ZydisDecoder decoder_;
    ElFile file_;
};