#pragma once

#include <cstdio>
#include <errno.h>
#include <fcntl.h>
#include <cstring>
#include <cstdlib>
#include <libelfin/elf/elf++.hh>

struct ElFile {

    using str = const char*;

    ElFile(str filename)
    {
        int fd = open(filename, O_RDONLY);
        if (fd < 0) {
            std::fprintf(stderr, "[-] %s: %s\n", filename, strerror(errno));
            std::exit(1);
        }
        e = new elf::elf(elf::create_mmap_loader(fd));
    }

    const elf::section getTextSection() {
        for (auto &section : e->sections()) {
            if (isTextSection(section)) {
                return section;
            }
        }

        fprintf(stderr, "Cannot find text section");
        exit(1);
    }


    bool isX86() {
        return getBitClass() == elf::elfclass::_32;
    }

    bool isX64() {
        return getBitClass() == elf::elfclass::_64;
    }


    ~ElFile() {
        if (e == nullptr) {
            delete e;
        }
    }

private:
    elf::elf *e;

    bool isTextSection(const elf::section &section) {
        auto name = section.get_name();
        return (name == ".text") || (name == ".code");
    }

    elf::elfclass getBitClass() {
        return e->get_hdr().ei_class;
    }
};