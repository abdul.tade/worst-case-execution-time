#pragma once

#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>

struct HexDump
{

    HexDump(int lineLength)
        : lineLength(lineLength)
    {
        if (lineLength <= 0)
        {
            std::cout << "Invalid line length\n";
            exit(1);
        }
    }

    void dump(const char *data, size_t size)
    {
        std::stringstream ss{};
        auto lineCount = size / static_cast<size_t>(lineLength);
        auto remainder = size % static_cast<size_t>(lineLength);
        if (lineCount == 0) {
            printLine(data, size);
            return;
        }
        for (size_t i = 0; i < lineCount; i++) {
            auto ptr = data + (i * lineLength);
            if ((remainder != 0) && (i == lineCount-1)) {
                printLine(ptr, remainder);
                return;
            }
            printLine(ptr, lineLength);
        }
    }

private:
    int lineLength;

    void printLine(const char *data, size_t size)
    {
        std::stringstream ss{};
        ss << std::setw(8) << '|';
        for (size_t i = 0; i < size; i++)
        {
            char c = *(data + i);
            if (isprint(c))
            {
                std::cout << "0x" << std::setw(8) << std::hex << (int)c << '\t';
                ss << std::setw(8) << c;
            }
            else
            {
                // printf("%8c", '.');
                std::cout << "0x" << std::setw(8) << std::hex << (int)c << '\t';
                ss << std::setw(8) << '.';
            }
        }
        ss << std::setw(8) << " |";
        std::cout << ss.str();
        putchar('\n');
    }
};