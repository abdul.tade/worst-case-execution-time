#include <iostream>
#include <libelfin/elf/elf++.hh>
#include "../lib/ArgumentParser.hpp"
#include "../lib/ElFile.hpp"
#include "../lib/Disassembler.hpp"

int main(int argc, char * argv[])
{
    std::unordered_set<std::string> required{
        "--filename"
    };

    ArgumentParser parser(3, required, std::unordered_set<std::string>{});
    std::span<char*> argSpan{argv, static_cast<size_t>(argc)};
    std::cout << "[+] Parsing command line arguments ...\n";
    parser.parseArgs(argSpan);

    std::cout << "[+] Collecting arguments ...\n";
    auto filename = parser.getValue<std::string>("--filename");

    std::cout << "[+] Parsing Elf file ...\n";

    ElFile file(filename.c_str());
    Disassembler disasm{file};

    auto instrs = disasm.decodeInstructions();

    for (auto & inst : instrs)
    {
        std::cout << "Instruction opcode: 0x" << std::hex << (int)inst.opcode << '\n';
        std::cout << "Operand Ids: \n";
        for (ZyanU8 i = 0; i < inst.operand_count; i++)   
        {
            std::cout << "\t" << std::hex << (int)inst.operands[i].id << '\n';
        }
    }
}